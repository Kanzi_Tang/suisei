import uuid

import mutagen

from suisei.action.action_execute import execute_action
from suisei.audio.audio_utils import get_audio_tag

from suisei.audio.audio_utils import get_image_by_file


def test_uuid():
    uid = str(uuid.uuid4())
    print(uid)


def test_refresh_info():
    args = {
        "side": None,
        "prefix": None
    }
    execute_action('refresh_audio_info', args)


def test_update_labels():
    args = {
        "side": None,
        "prefix": None,
        "audio_id": "64cf504c-560d-40ae-8276-c61a10ee99ce",
        "add_labels": "tes1,test2",
        "delete_labels": "test2",
    }
    execute_action('update_labels', args)


def test_get_all_audio():
    args = {
        "side": None,
        "prefix": None
    }
    execute_action('get_all_audio_info', args)


def test_mutagen_file():
    mp3 = 'D:\\OneDrive\\Music\\陰陽座\\雷神創世\\神鳴忍法帖.mp3'
    flac = 'D:\\OneDrive\\Music\\Aimer\\花の唄_ONE_六等星の夜 Magic Blue ver\\01 花の唄.flac'
    ogg = 'D:\\OneDrive\\Music\\神劍闖江湖\\るろうに剣心-明治剣客浪漫譚-\\1-3 の純情な感情.ogg'

    file = ogg

    tags = mutagen.File(file)
    print(type(tags))
    print(tags.pprint())
    info = get_audio_tag(file)
    print(info)

    image = get_image_by_file(file)
    print(image)


def test_get_images():
    args = {
        "audio_id": "6d8c663d-88f2-42ff-bcdd-6288385769c5,3f17bff0-1e79-424a-816b-6469c0fc0024"
    }
    result = execute_action('get_audio_images', args)
    print(result)


if __name__ == '__main__':
    test_get_images()
