# Audio Play #



### What is this repository for? ###

* Bootstrap 5.2
* Python 3.6
* Flask 2.0.1
* Jquery 3.3.1

### API ###

Get Audios
* GET /audio
* Get all song information

Run Refresh
* POST /audio/refresh
* Refresh audio information from audio file

Get Image
* GET /audio/image
* Get song cover from file metadata

Get Albums
* GET /audio/albums
* Get all album information

Update labels
* GET /audio/labels/update
* Update song label


Get All labels
* GET /audio/labels/all
* Get all labels 

### Run ###

run app.py , it will create local db if it is not existed.
