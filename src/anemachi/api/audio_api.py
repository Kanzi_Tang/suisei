import threading

from flask import Blueprint, request
from flask import make_response

from anemachi.utils.job_utils import check_flag
from anemachi.utils.job_utils import delete_flag
from anemachi.utils.socket_utils import socket_io
from anemachi.utils.web_utils import api_output_handle
from suisei.action.action_execute import execute_action
from suisei.config.configuration import server_static
from suisei.action.get_albums import GetAlbums
from suisei.action.get_audios_image import GetAudiosImage
from suisei.action.refresh_audio_info import RefreshAudioInfo
from suisei.common.log_utils import get_logger

audio_api_blueprints = Blueprint('audio_api', __name__)

_logger = get_logger(__name__)

job_refresh_flag = 'job_flag_refresh.txt'


@audio_api_blueprints.route('/refresh', methods=['POST'])
def refresh_audio():
    def refresh():
        try:
            execute_action(RefreshAudioInfo())
        finally:
            delete_flag(job_refresh_flag)
            socket_io.emit('refresh_album', {'data': "1"})

    def run():
        if check_flag(job_refresh_flag):
            _logger.warn("Refresh job is running, please wait")
            return False
        else:
            thread = threading.Thread(target=refresh)
            thread.start()

            return True

    return api_output_handle(run)


@audio_api_blueprints.route('/image', methods=['Get'])
def get_image():
    image = execute_action(GetAudiosImage(), request.args)

    if image is None:
        with open(server_static + "/img/default_cover.jpg", 'rb') as f:
            image = f.read()

    response = make_response(image)
    response.headers.set('Content-Type', 'image/jpeg')

    return response


@audio_api_blueprints.route('/albums', methods=['GET'])
def get_albums():
    def run():
        albums = execute_action(GetAlbums(), request)

        results = []
        for album in albums:
            result = album
            if result.get('wm_id'):
                del result['wm_id']
            if result.get('mcdi'):
                del result['mcdi']
            results.append(result)

        return results

    return api_output_handle(run)
