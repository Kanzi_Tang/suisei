import threading
import time

from flask import Blueprint, request

from anemachi.utils.socket_utils import socket_io
from anemachi.utils.web_utils import api_output_handle
from suisei.action.play_list_action import add_play_list, delete_play_list, get_next_song, get_play_list, \
    get_play_now_song, \
    get_play_song_data
from suisei.common.log_utils import get_logger
from suisei.play.player import player

_logger = get_logger(__name__)

play_blueprints = Blueprint('player', __name__)


@play_blueprints.route("/list/add", methods=['POST'])
def add():
    def run():
        add_play_list(request.json)
        song_list = get_play_list()
        _logger.debug(f'Song list: {str(song_list)}')
        socket_io.emit('play_list', {'data': song_list})
        return True

    return api_output_handle(run)


@play_blueprints.route("/list", methods=['Get'])
def get_list():
    def run():
        song_list = get_play_list()
        _logger.debug(f'Song list: {str(song_list)}')
        socket_io.emit('play_list', {'data': song_list})
        return song_list

    return api_output_handle(run)


@play_blueprints.route("/list/delete", methods=['DELETE'])
def delete_list():
    def run():
        delete_play_list(request.json)
        song_list = get_play_list()
        _logger.debug(f'Song list: {str(song_list)}')
        socket_io.emit('play_list', {'data': song_list})
        return True

    return api_output_handle(run)


def watch_play_time_pos():
    while not player.is_play_finish():
        time.sleep(1)
        result = {
            "pos": player.get_pos()
        }
        socket_io.emit('player_time', {'data': result})

    if play_next_song():
        watch_play_time_pos()
    else:
        stop_play()


@play_blueprints.route("/play", methods=['POST'])
def play():
    def run():
        song = get_play_song_data(request.json)
        if song:
            player.play_file(song.file_path)

            song_list = get_play_list()
            _logger.debug(f'Song list: {str(song_list)}')
            socket_io.emit('play_list', {'data': song_list})

            play_song = get_play_now_song()
            play_song["playStatus"] = player.player_status
            socket_io.emit('player', {'data': play_song})

            thread = threading.Thread(target=watch_play_time_pos)
            thread.start()

        return True

    return api_output_handle(run)


@play_blueprints.route("/", methods=['GET'])
def player_info():
    def run():
        song = get_play_now_song()
        if song:
            if player is None:
                player.filename = song.get('filename')

            song["playStatus"] = player.player_status

            socket_io.emit('player', {'data': song})
        return song

    return api_output_handle(run)


@play_blueprints.route("/pause", methods=['POST'])
def pause():
    def run():
        player.pause()
        song = get_play_now_song()
        song["playStatus"] = player.player_status
        socket_io.emit('player', {'data': song})
        return True

    return api_output_handle(run)


def stop_play():
    player.stop()
    song = get_play_now_song()
    song["playStatus"] = player.player_status
    socket_io.emit('player', {'data': song})


@play_blueprints.route("/stop", methods=['POST'])
def stop():
    def run():
        stop_play()
        return True

    return api_output_handle(run)


@play_blueprints.route("/changeVolume", methods=['POST'])
def change_volume():
    def run():
        if request.json:
            volume = request.json.get('volume')
            if volume:
                player.set_volume(float(volume))
                result = {
                    "volume": player.volume
                }
                socket_io.emit('player_volume', {'data': result})
                return True
        return False

    return api_output_handle(run)


@play_blueprints.route("/volume", methods=['GET'])
def get_volume():
    def run():
        result = {
            "volume": player.volume
        }
        socket_io.emit('player_volume', {'data': result})
        return result

    return api_output_handle(run)


@play_blueprints.route("/changePos", methods=['POST'])
def change_pos():
    def run():
        if request.json:
            pos = request.json.get('pos')
            if pos:
                # get_pos not change, it bug
                # player.set_pos(pos)
                return True
        return False

    return api_output_handle(run)


def play_next_song():
    list_id = get_next_song()
    if list_id:
        song = get_play_song_data({"listId": list_id})
        if song:
            player.stop()
            player.play_file(song.file_path)

            song_list = get_play_list()
            _logger.debug(f'Song list: {str(song_list)}')
            socket_io.emit('play_list', {'data': song_list})

            play_song = get_play_now_song()
            play_song["playStatus"] = player.player_status
            socket_io.emit('player', {'data': play_song})

            return True

    return False


@play_blueprints.route("/playNext", methods=['POST'])
def play_next():
    def run():
        if play_next_song():
            thread = threading.Thread(target=watch_play_time_pos)
            thread.start()
        return True

    return api_output_handle(run)
