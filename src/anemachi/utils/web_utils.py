from flask import jsonify

from suisei.common.log_utils import get_logger

_logger = get_logger(__name__)

OFFSET = 0
LIMIT = 1000


def return_with_page(data, input_offset=None, input_limit=None):
    if not data or not isinstance(data, list):
        return data

    offset = OFFSET
    limit = LIMIT
    if input_offset:
        offset = int(input_offset)
    if input_limit:
        limit = int(input_limit)

    total_counts = len(data)
    pages = [data[i:i + limit] for i in range(0, total_counts, limit)]

    page_num = int(offset / limit)
    total_pages = len(pages)
    result = {"total_counts": total_counts, "total_pages": total_pages}

    if page_num >= total_pages:
        result["now_page"] = total_pages
        data = pages[total_pages - 1]
        result["data"] = data
    else:
        result["now_page"] = page_num + 1
        data = pages[page_num]
        result["data"] = data

    return result


def api_output_handle(func):
    try:
        result = func()
        return jsonify(code=0, data=result)
    except Exception as ex:
        _logger.error("Api error : ", ex)
        return jsonify(code=1, error_message=str(ex)), 400
