import os

from suisei.common.log_utils import get_logger

_logger = get_logger(__name__)


def check_flag(file):
    if os.path.exists(file):
        return True
    else:
        try:
            with open(file, 'w') as f:
                f.write('Create flag')
        except Exception as ex:
            _logger.error(f'Create flag error:', ex)
            return False


def delete_flag(file):
    if os.path.exists(file):
        os.remove(file)
    else:
        print("The file does not exist")
