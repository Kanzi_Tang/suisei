var socket

function convertTime(second){
    const hours = Math.floor((second % 86400) / 3600);
    const minutes = Math.floor(((second % 86400) % 3600) / 60);
    const seconds = Math.floor(((second % 86400) % 3600) % 60);

    var output = '';

    if (hours > 0){
        output += hours+':'
    }
    if (minutes > 0){
        output += minutes+':'
    }
    if (seconds > 0){
        output += seconds
    }
    return output
}

function generatePlayListView(songList){
    var view = ''

    var index = 1
    songList.forEach(function (song) {
        view += '<div id="playList'+index+'" class="border-bottom border-2 border-dark">'
        if (song.play_status > 1) {
            view += ' >>  '
        };
        view += '<div listId='+song.list_id+' song_id='+song.song_id+' id="listOf'+song.list_id+'" style="padding-left: 0.5rem;"> '
        view += '<img src="/static/img/icons8-circled-play-30.png" style="height: 2rem;width: 2rem;" onclick="playSongFromList(\'listOf'+song.list_id+'\')">'
        view += '<font size="3">'+song.song+'</font>&nbsp;&nbsp;&nbsp;&nbsp;'
        view += '<img src="/static/img/icons8-minus-48.png" style="height: 1rem;width: 1rem;" onclick="deleteFromPlayList(\'listOf'+song.list_id+'\')">'
        view += '<br><font size="2">'+song.album +'&nbsp;&nbsp;-&nbsp;&nbsp;'+song.artist+'</font><br>'
        view += '<font size="1">'+convertTime(song.duration)+'</font>'
        view += '</div>'
        view += '</div>'
        index++
    });

    $('#playList').html(view);
}

function getPlayList(){
    $.ajax({
        method: "get",
        headers: {
            "Content-Type": "application/json"
        },
        url: "/player/list",
        success: function (response) {
            // return type is json
            var songList = response['data'];
            generatePlayListView(songList);
        }
    })
}

function initPlay(){
    socket = io.connect();
    getPlayList()
    getPlayerInfo()
    getVolume()
}

function addToPlayList(divId) {
    var songDiv = document.getElementById(divId);
    var id = songDiv.getAttribute('songId')
    var dataJSON = {};
    dataJSON["songIds"] = [id];

    $.ajax({
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        url: "/player/list/add",
        dataType: "json",
        data: JSON.stringify(dataJSON)
    })

}

function deleteFromPlayList(divId) {
    var listDiv = document.getElementById(divId);
    var id = listDiv.getAttribute('listId')
    var dataJSON = {};
    dataJSON["listIds"] = [id];
    callDeletePlayList(dataJSON)
}

function deleteAllPlayList() {
    var dataJSON = {};
    dataJSON["all"] = true;
    callDeletePlayList(dataJSON)
}

function callDeletePlayList(dataJson){
    $.ajax({
        method: "delete",
        headers: {
            "Content-Type": "application/json"
        },
        url: "/player/list/delete",
        dataType: "json",
        data: JSON.stringify(dataJson)
    });
}

function callPlay(dataJson){
    $.ajax({
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        url: "/player/play",
        dataType: "json",
        data: JSON.stringify(dataJson)
    });
}

function playSongFromList(divId){
    var listDiv = document.getElementById(divId);
    var id = listDiv.getAttribute('listId')
    var dataJSON = {};
    dataJSON["listId"] = id;
    callPlay(dataJSON)
}

function playSong(divId){
    var songDiv = document.getElementById(divId);
    var id = songDiv.getAttribute('songId')
    var dataJSON = {};
    dataJSON["songId"] = id;
    callPlay(dataJSON)
}

