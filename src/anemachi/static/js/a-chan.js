$(document).ready(function(){
    socket.on('play_list', function(msg) {
        var songList = msg.data
        generatePlayListView(songList)
    });

    socket.on('player',function(msg) {
        var player_info = msg.data
        if (player_info !== null){
            updatePlayer(player_info)
        }
    });

    socket.on('player_volume',function(msg) {
        var volume = msg.data
        if (volume !== null){
            updateVolume(volume)
        }
    });

    socket.on('player_time',function(msg) {
        var time = msg.data
        if (time !== null){
            updateTimeSlider(time)
        }
    });

    socket.on('refresh_album',function(msg) {
        var data = msg.data
        if (data !== null){
            getAlbums()
        }
    });

});