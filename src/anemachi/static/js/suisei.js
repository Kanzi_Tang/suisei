function getAlbums() {
    var albumView = document.getElementById("leftView");
    $.ajax({
        method: "get",
        headers: {
            "Content-Type": "application/json"
        },
        url: "/audio/albums",
        success: function (response) {
            // return type is json
            var result = response['data'];

            var view = '';
            view += '<br>';
            view += '<div class="row align-items-start">';

            var albumIndex = 1
            result.forEach(function (album) {
                view += '<div class="col">';

                view += '<div class="card" style="width: 200px;">';
                view += '<img class="card-img-top" style="width: 200px; height: 200px;"  src="/audio/image?audio_id=' + album.songs[0].id + '">';
                view += '<div class="card-body">';
                view += '<h5 class="card-title">' + album.album + '</h5>';
                view += '<p class="card-text text-break">Artist:' + album.artist + '</p>';

                view += '<button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#Songs' + albumIndex + '" aria-expanded="false" aria-controls="Songs' + albumIndex + '">'
                view += 'Songs</i>'
                view += '</button>'
                view += '</div>'; // card-body

                view += '<div class="collapse multi-collapse" id="Songs' + albumIndex + '">'
                view += '<ul class="list-group">';
                var index = 1
                album.songs.forEach(function (song) {
                    view += '<li class="list-group-item" name=' + song.song + ' album=' + song.album + ' songId=' + song.id + ' duration=' + song.duration + '>';
                    view += '<img src="/static/img/icons8-plus-math-30.png" onclick="addToPlayList(\'song'+song.id+'\')"> '
                    view += '<img src="/static/img/icons8-circled-play-30.png" onclick="playSong(\'song'+song.id+'\')"> '
                    view += '<div id="song'+song.id+'"  songId=' + song.id+'></div>'
                    view += '#' + index + '  ' + song.song;
                    view += '</li>';
                    index++;
                });
                view += '</ul>';

                view += '</div>'

                view += '</div>'; // card
                view += '</div>'; // col

                albumIndex++;
            });

            view += '</div></div>';
            albumView.innerHTML = view;
        },
        error: function (response) {
            albumView.innerHTML = JSON.stringify(response['responseJSON']);
        }
    });
}

function refresh() {
    $.ajax({
        method: "post",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        url: "/audio/refresh",
        success: function (response) {
            // return type is json
            var result = response
            albumView.innerHTML = JSON.stringify(result['data']);
        },
        error: function (response) {
            albumView.innerHTML = JSON.stringify(response['responseJSON']);
        }
    });
}

function init() {
    initPlay()
    getAlbums()
}