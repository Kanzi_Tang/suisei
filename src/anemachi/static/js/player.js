function updatePlayer(info){
    updateToPlayBottom(info)
    updateCover(info)
    updateNameWithDuration(info)
}

function updateNameWithDuration(info){
        var div = document.getElementById("songName");
        div.setAttribute("duration",info.duration);
        div.innerHTML = info.songName
}

function updateToPlayBottom(info) {
    var view = '<img style="width: 2rem; height: 2rem;" src="static/img/play.png">'
    if (info.playStatus == 0) {
        view = '<img style="width: 2rem; height: 2rem;" src="static/img/play.png" onclick=playSongBottom('+info.listId+')>'
    }
    if (info.playStatus == 1){
        view = '<img style="width: 2rem; height: 2rem;" src="static/img/pause.png" onclick=callPause()>'
    }
    if (info.playStatus == 2){
        view = '<img style="width: 2rem; height: 2rem;" src="static/img/play.png" onclick=callPause()>'
    }
    $('#playerPlay').html(view);
}

function updateCover(info) {
    var coverDiv = document.getElementById('playerCover');
    var id = coverDiv.getAttribute('songId')
    if (id != info.songId){
        var view = '<img style="width: 4rem; height: 4rem;" songId="'+info.songId+'" src="/audio/image?audio_id=' + info.songId + '">'
        coverDiv.innerHTML = view
    }
}

function playSongBottom(listId){
    if (listId != null){
        var dataJSON = {};
        dataJSON["listId"] = listId;
        callPlay(dataJSON)
    }
}

function callPause(){
    $.ajax({
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        url: "/player/pause",
        dataType: "json"
    });
}

function callStop(){
    $.ajax({
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        url: "/player/stop",
        dataType: "json"
    });
}

function callNext(){
    $.ajax({
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        url: "/player/playNext",
        dataType: "json"
    });
}

function getPlayerInfo(){
    $.ajax({
        method: "get",
        headers: {
            "Content-Type": "application/json"
        },
        url: "/player",
        dataType: "json",
        success: function (response) {
            // return type is json
            var result = response['data'];
            updatePlayer(result)
        }
    });
}

function getVolume(){
    $.ajax({
        method: "get",
        headers: {
            "Content-Type": "application/json"
        },
        url: "/player/volume",
        dataType: "json",
        success: function (response) {
            // return type is json
            var result = response['data'];
            updateVolume(result)
        }
    });
}

function updateVolume(data) {
    var volume = data.volume
    if (volume != null && volume >= 0) {
        volume = volume*100
        var volumeDiv = document.getElementById('playerVolumeSlider');
        volumeDiv.value=volume
    }
}

function changeVolume(){
    var slider = document.getElementById("playerVolumeSlider")
    var value = slider.value / 100;
    var dataJSON = {};
    dataJSON["volume"] = value;
    $.ajax({
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        url: "/player/changeVolume",
        dataType: "json",
        data: JSON.stringify(dataJSON)
    });
}

function updateTimeSlider(data) {
    var pos = data.pos
    if (pos != null && pos >= 0) {
        var div = document.getElementById("songName");
        var duration = div.getAttribute('duration')
        if (duration != null){
            var time = (pos/(1000*duration))*100
            if (time > 100){
                time = 100
            }
            if (time < 0){
                time = 0
            }
            var posDiv = document.getElementById('playerTimeSlider');
            posDiv.value=time.toFixed(2)
        }
    }
}

function changeTimePos(){
    var slider = document.getElementById("playerTimeSlider")
    var value = slider.value / 100;

    var songInfo = document.getElementById("songName")
    var duration = songInfo.getAttribute('duration')
    if (duration != null) {
    var dataJSON = {};
        dataJSON["pos"] = value*duration;
        $.ajax({
            method: "post",
            headers: {
                "Content-Type": "application/json"
            },
            url: "/player/changePos",
            dataType: "json",
            data: JSON.stringify(dataJSON)
        });
    }
}