from flask import Blueprint
from flask import render_template

index_blueprints = Blueprint('index', __name__)


@index_blueprints.route('/')
def index():
    return render_template('index.html')
