import os
import sys
from os.path import join
from pathlib import Path

from flask import Flask

from anemachi.utils.socket_utils import socket_io
from suisei.config.configuration import server_host
from suisei.config.configuration import server_port
from suisei.config.configuration import server_static
from suisei.config.configuration import server_template
from suisei.config.configuration import local_database_path
from suisei.common.log_utils import get_logger
from suisei.database.sqlalchemy_init import db
from suisei.database.import_model import ModelList
from suisei.database.sqlalchemy_init import migrate

_logger = get_logger(__name__)

def create_app():
    app_flack = Flask(__name__,
                      template_folder=server_template,
                      static_folder=server_static)

    app_flack.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
    app_flack.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
    app_flack.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
    app_flack.config['SQLALCHEMY_ECHO'] = True
    app_flack.config['SQLALCHEMY_DATABASE_URI'] = local_database_path
    app_flack.config['DEBUG'] = True

    from anemachi.api.audio_api import audio_api_blueprints
    from anemachi.web.index import index_blueprints
    from anemachi.api.play_api import play_blueprints

    app_flack.register_blueprint(audio_api_blueprints, url_prefix='/audio')
    app_flack.register_blueprint(index_blueprints, url_prefix="/")
    app_flack.register_blueprint(play_blueprints, url_prefix="/player")

    socket_io.init_app(app_flack)

    return app_flack


ModelList


def init_path():
    work_path = str(Path(os.path.abspath(__file__)).parent)
    sys.path.append(work_path)
    src_path = work_path + '/src'
    sys.path.append(src_path)
    for root, dirs, files in os.walk(src_path):
        for f in dirs:
            full_path = join(root, f)
            sys.path.append(full_path)


# if __name__ == '__main__':
init_path()
for sys_path in sys.path:
    _logger.info(f'System path: {str(sys_path)}')
os.environ['FLASK_ENV'] = 'development'
app = create_app()
db.app = app
db.init_app(app)
migrate.init_app(app, db)
db.create_all()
# app.run(host=server_host, port=server_port)
socket_io.run(app, host=server_host, port=server_port)
