import os
import time
from multiprocessing import Pool

from dateutil import parser
from examples import file

from suisei.audio.audio_utils import get_audio_files
from suisei.common.log_utils import get_logger
from suisei.config.configuration import default_song_file_path
from suisei.config.configuration import cpu_pool_num
from suisei.database.sqlalchemy_init import db
from suisei.song.song import Song
from suisei.song.song import create_song

_logger = get_logger(__name__)


def convert_to_song_class(audio_file):
    return create_song(audio_file)


def refresh_audio_db():
    file_path = default_song_file_path
    audio_files = get_audio_files(file_path)

    delete_songs = []
    songs = Song.query.all()
    for song in songs:
        if os.path.exists(song.file_path):
            update_song_form_file(song)
            audio_files.remove(song.file_path)
        else:
            delete_songs.append(song.id)

    if len(delete_songs) > 0:
        for d_song in delete_songs:
            d_song.delete()
        db.session.commit()

    insert_new_audio(cpu_pool_num, audio_files)

    _logger.info("Refresh done")


def insert_new_audio(pool_num, audio_files):
    _logger.info(f'Pool num {pool_num}')
    pool = Pool(processes=int(pool_num))
    songs = pool.map(convert_to_song_class, audio_files)

    if len(songs) > 0:
        db.session.add_all(songs)
        db.session.commit()
    else:
        _logger.info("Not new audio")


def update_song_form_file(song):
    file_path = song.file_path
    file_update_date = song.file_update_date
    local_file_update_time = parser.parse(time.ctime(os.path.getmtime(file_path)))

    if local_file_update_time > file_update_date:
        song.set_by_mp3_tag(file)
        song.file_update_date = local_file_update_time
        song.update()


class RefreshAudioInfo:
    def execute(self):
        try:
            refresh_audio_db()
            return {"refresh_audio_db": True}
        except Exception as ex:
            _logger.error(f'Run refresh error : ', ex)
            return {"refresh_audio_db": False}
