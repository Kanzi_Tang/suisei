from suisei.common.log_utils import get_logger
from suisei.database.sqlalchemy_init import db
from suisei.song.song import Song

_logger = get_logger(__name__)

class AudioSelectInput:
    def __init__(self):
        self.ids = None
        self.songs = None
        self.albums = None
        self.artists = None

    def convert_to_input(self, parameter):
        if parameter:
            if parameter.get("ids"):
                self.ids = parameter.get("ids")
            if parameter.get("songs"):
                self.songs = parameter.get("songs")
            if parameter.get("albums"):
                self.albums = parameter.get("albums")
            if parameter.get("artists"):
                self.artists = parameter.get("artists")

def get_album_info(offset, limit,
                   album_input=AudioSelectInput(), include_labels=None, exclude_labels=None):
    get_song_query = """
        SELECT s.* FROM song s
        WHERE 1=1 {whereClause}
        GROUP BY s.album
        ORDER BY s.track_int
        LIMIT :limit OFFSET :offset;
    """
    get_song_query = get_song_query.replace("{whereClause}", generate_query(album_input))
    first_songs = Song.query.from_statement(
        db.text(get_song_query)
    ).params(
        album_input.__dict__, offset=offset, limit=limit
    ).all()

    albums = []
    for first_song in first_songs:
        album_name = first_song.album
        artist_name = first_song.artist
        album_songs = Song.query.filter_by(album=album_name).order_by(Song.track_int).all()
        data = {
            "album": album_name,
            "artist": artist_name,
            "songs": [album_song.as_dict() for album_song in album_songs]
        }
        albums.append(data)

    return albums


def generate_query(album_input=AudioSelectInput()):
    sql = ""
    if album_input.ids:
        sql += " AND s.id IN :ids "
    if album_input.songs:
        sql += " AND s.song IN :songs "
    if album_input.albums:
        sql += " AND s.album IN :albums "
    if album_input.artists:
        sql += " AND s.artist IN :artists "

    return sql

class GetAlbums:
    def __init__(self):
        self.include_labels = set()
        self.exclude_labels = set()
        self.offset = 0
        self.limit = 1000
        self.condition = AudioSelectInput()

    def set_args(self, request):
        args = request.args

        if args.get('include_labels'):
            labels = set(args.get('include_labels').split(','))
            self.include_labels.update(labels)

        if args.get('exclude_labels'):
            labels = set(args.get('exclude_labels').split(','))
            self.exclude_labels.update(labels)

        if args.get('offset'):
            self.offset = int(args.get('offset'))

        if args.get('limit'):
            self.limit = int(args.get('limit'))

        try:
            body = request.json
            if body:
                if body.get('condition'):
                    self.condition.convert_to_input(body.get('condition'))
        except Exception as ex:
            _logger.error("Get body error : ", ex)

    def execute(self):
        albums = get_album_info(self.offset, self.limit, self.condition)
        return albums
