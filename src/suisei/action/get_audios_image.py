from suisei.audio.audio_utils import AUDIO_COLUMN_IMAGE
from suisei.audio.audio_utils import get_image_by_file
from suisei.common.log_utils import get_logger
from suisei.song.song import Song

_logger = get_logger(__name__)


class GetAudiosImage:
    def __init__(self):
        self.audio_id = None

    def set_args(self, args):
        if args.get('audio_id'):
            self.audio_id = args.get('audio_id')

    def execute(self):
        song = Song.query.filter_by(id=self.audio_id).first()

        image = None
        if song and song.file_path:
            image = get_image_by_file(song.file_path)
            if image:
                return image.get(AUDIO_COLUMN_IMAGE)

        return image
