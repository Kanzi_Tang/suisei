from suisei.common.log_utils import get_logger

_logger = get_logger(__name__)


def execute_action(action, args=None):
    if action:
        _logger.info(f'Do {action.__class__.__name__} action with args {str(args)}')
        if args:
            action.set_args(args)
        return action.execute()

    else:
        message = f'Not find action : {action.__class__.__name__}'
        _logger.warn(message)
        return message
