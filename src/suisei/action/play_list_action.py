from sqlalchemy import func
from suisei.common.log_utils import get_logger
from suisei.play.play_list import PlayList
from suisei.song.song import Song
from suisei.database.sqlalchemy_init import db

_logger = get_logger(__name__)


def get_play_list():
    p_list = PlayList.query.order_by(PlayList.sequence).all()

    list_of_song_id = [p.song_id for p in p_list]

    if list_of_song_id and len(list_of_song_id) > 0:
        songs = Song.query.filter(Song.id.in_(list_of_song_id)).all()

    list_pair = []
    for p in p_list:
        for song in songs:
            if p.song_id == song.id:
                list_pair.append((song, p))

    if len(list_pair) > 0:
        return [as_play_list_output(song, play_list) for song, play_list in list_pair]
    else:
        return []


def as_play_list_output(song, play_list):
    return {
        "list_id": play_list.id,
        "play_status": play_list.play_status,
        "song_id": song.id,
        "song": song.song,
        "album": song.album,
        "duration": song.duration,
        "artist": song.artist,
    }


def add_play_list(songs):
    song_ids = songs.get('songIds')
    if song_ids:
        index = 1
        lastSequence = PlayList.query.order_by(PlayList.sequence.desc()).first()
        if lastSequence:
            index = lastSequence.sequence + 1

        try:
            for id in song_ids:
                db.session.add(PlayList(song_id=id, sequence=index))
                index += 1

            db.session.commit()
        except Exception as ex:
            _logger.error('Add play list error:', ex)
            db.session.rollback()
            return False

        return True
    else:
        _logger.warn('Not find song ids from input')
        return False


def delete_play_list(data):
    if data.get('all') == True:
        try:
            num_rows_deleted = db.session.query(PlayList).delete()
            db.session.commit()
            _logger.info(f'Delete all play list: {str(num_rows_deleted)}')
        except Exception as ex:
            _logger.error('Delete all play list error:', ex)
            db.session.rollback()
        return True
    else:

        list_ids = data.get('listIds')
        if list_ids:
            PlayList.query.filter(PlayList.id.in_(list_ids)).delete()
            db.session.commit()
            _logger.info(f'Delete list id: {str(list_ids)}')

        return True


def get_play_song_data(input):
    play_now_list = PlayList.query.filter(PlayList.play_status == 1).first()
    if play_now_list:
        play_now_list.play_status = 0
        db.session.commit()

    song_id = None
    if input.get('listId'):
        play_list_song = PlayList.query.filter(PlayList.id == input.get('listId')).first()
        if play_list_song:
            song_id = play_list_song.song_id
            play_list_song.play_status = 1
            db.session.commit()

    if input.get('songId'):
        song_id = input.get('songId')
        add_play_list({"songIds": [song_id]})
        list_song = PlayList.query.order_by(PlayList.sequence.desc()).first()
        list_song.play_status = 1
        db.session.commit()

    if song_id:
        return Song.query.filter(Song.id == song_id).first()

    return None


def get_play_now_song():
    song_id = None
    play_list_song = PlayList.query.filter(PlayList.play_status == 1).first()
    if play_list_song:
        song_id = play_list_song.song_id

    if song_id:
        song = Song.query.filter(Song.id == song_id).first()
        return {
            "listId": play_list_song.id,
            "songId": song_id,
            "songName": song.song,
            "filename": song.file_path,
            "duration": song.duration
        }

    return None


def get_next_song():
    play_list_songs = PlayList.query.filter(PlayList.play_status == 1).first()
    list_id = None
    if play_list_songs:
        next_song = PlayList.query.filter(PlayList.sequence > play_list_songs.sequence).order_by(PlayList.sequence).first()
        if next_song:
            list_id = next_song.id

    return list_id
