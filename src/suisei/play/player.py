from email.policy import default
import os
import pygame

NOT_PLAY = 0
PLAY_ING = 1
PAUSE_ING = 2
STOP_PLAY = 3

DEFAULT_VOLUME = 0.9


class Player:
    player_status = NOT_PLAY
    filename = None

    def __init__(self):
        self.filename = None
        self.player = pygame
        self.player.init()
        self.player.mixer.init(frequency=96000, size=32, allowedchanges=pygame.AUDIO_ALLOW_ANY_CHANGE)
        self.player_status = NOT_PLAY
        self.volume = DEFAULT_VOLUME

    def play_file(self, new_filename):
        if not os.path.exists(new_filename):
            return False

        if self.filename == new_filename:
            if self.player_status == PLAY_ING:
                return False

            if self.player_status == PAUSE_ING:
                self.player.mixer.music.unpause()
                return True
        else:
            self.stop()

        self.player.mixer.music.load(new_filename)
        self.filename = new_filename
        self.player.mixer.music.set_volume(self.volume)
        self.player.mixer.music.play()
        self.player_status = PLAY_ING

        return True

    def pause(self):
        if self.player_status == PLAY_ING:
            self.player.mixer.music.pause()
            self.player_status = PAUSE_ING
            return True

        if self.player_status == PAUSE_ING:
            self.player.mixer.music.unpause()
            self.player_status = PLAY_ING

    def stop(self):
        self.player.mixer.music.stop()
        self.player_status = NOT_PLAY

    def is_play_finish(self):
        if self.player.mixer.music.get_pos() > 0:
            return False
        else:
            self.player_status = NOT_PLAY
            return True
            return False

    def set_volume(self, new_volume):
        self.player.mixer.music.set_volume(new_volume)
        self.volume = new_volume

    def set_pos(self, pos):
        self.player.mixer.music.set_pos(float(pos))

    def get_pos(self):
        return self.player.mixer.music.get_pos()


player = Player()

# if __name__=='__main__':
#     filename = 'D:/OneDrive/Music/久石讓/Dream Songs_ The Essential Joe Hisaishi/01_01_One Summer_s Day (from _Spirited Away_).flac'
#     pygame.mixer.music.load(filename)
#     # thread = threading.Thread(target=RUN)
#     # thread.start()

#     globals()['play_status']=1

#     sleep(5)

#     globals()['play_status']=2

#     sleep(5)

#     globals()['play_status']=3

#     sleep(5)

#     globals()['play_status']=4

#     sleep(5)

#     filename2 = 'D:/OneDrive/Music/水瀬いのり/Ready Steady Go!/02 Happy Birthday.flac'
#     pygame.mixer.music.load(filename2)

#     globals()['play_status']=1

#     sleep(5)

#     globals()['play_status']=4
