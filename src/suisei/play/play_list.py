from suisei.database.sqlalchemy_init import db

class PlayList(db.Model):
    __tablename__ = 'play_list'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    song_id = db.Column(db.Integer, nullable=False)
    sequence = db.Column(db.Integer, nullable=False)
    play_status = db.Column(db.Integer, default=0, nullable=False)

    def __init__(self, song_id, sequence, play_status=None):
        self.song_id = song_id
        self.sequence = sequence
        if play_status:
            self.play_status = play_status
        else:
            self.play_status = 0

