import os
from datetime import datetime
import time

from suisei.audio.audio_utils import AUDIO_COLUMN_ALBUM
from suisei.audio.audio_utils import AUDIO_COLUMN_ALBUM_ARTIST
from suisei.audio.audio_utils import AUDIO_COLUMN_ARTIST
from suisei.audio.audio_utils import AUDIO_COLUMN_COMPOSER
from suisei.audio.audio_utils import AUDIO_COLUMN_DATE
from suisei.audio.audio_utils import AUDIO_COLUMN_GENRE
from suisei.audio.audio_utils import AUDIO_COLUMN_MCDI
from suisei.audio.audio_utils import AUDIO_COLUMN_SONG
from suisei.audio.audio_utils import AUDIO_COLUMN_TRACK
from suisei.audio.audio_utils import AUDIO_COLUMN_WM_ID
from suisei.audio.audio_utils import get_audio_duration
from suisei.audio.audio_utils import get_audio_tag
from suisei.database.sqlalchemy_init import db
from dateutil.parser import parse


class Song(db.Model):
    __tablename__ = 'song'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    labels = db.Column(db.String(1024), nullable=True)
    track_str = db.Column(db.String(10), nullable=True)
    track_int = db.Column(db.Integer, nullable=True)
    file_path = db.Column(db.String(1024), nullable=True)
    song = db.Column(db.String(1024), nullable=True)
    album = db.Column(db.String(1024), nullable=True)
    artist = db.Column(db.String(1024), nullable=True)
    album_artis = db.Column(db.String(1024), nullable=True)
    genre = db.Column(db.String(256), nullable=True)
    wm_id = db.Column(db.String(256), nullable=True)
    composer = db.Column(db.String(256), nullable=True)
    mcdi = db.Column(db.String(256), nullable=True)
    publish_date = db.Column(db.String(128), nullable=True)
    file_update_date = db.Column(db.DateTime, nullable=True)
    duration = db.Column(db.Integer, nullable=True)
    update_date = db.Column(
        db.DateTime, onupdate=datetime.now(), default=datetime.now(), nullable=True)

    def __init__(self, id, labels, track_str, track_int,
                 file_path, song, album, artist, album_artis, genre, wm_id,
                 composer, mcdi, publish_date, file_update_date,
                 duration, update_date):
        self.id = id
        self.labels = labels
        self.track_str = track_str
        self.track_int = track_int
        self.file_path = file_path
        self.song = song
        self.album = album
        self.artist = artist
        self.album_artis = album_artis
        self.genre = genre
        self.wm_id = wm_id
        self.composer = composer
        self.mcdi = mcdi
        self.publish_date = publish_date
        self.file_update_date = file_update_date
        self.duration = duration
        self.update_date = update_date

    def set_by_mp3_tag(self, file):
        if file and isinstance(file, str):
            info = get_audio_tag(file)
            if any(info):
                self.file_path = file
                self.song = info.get(AUDIO_COLUMN_SONG)
                self.album = info.get(AUDIO_COLUMN_ALBUM)
                self.artist = info.get(AUDIO_COLUMN_ARTIST)
                self.album_artis = info.get(AUDIO_COLUMN_ALBUM_ARTIST)
                if info.get(AUDIO_COLUMN_GENRE):
                    self.genre = info.get(AUDIO_COLUMN_GENRE)
                if info.get(AUDIO_COLUMN_WM_ID):
                    self.wm_id = str(info.get(AUDIO_COLUMN_WM_ID))
                self.composer = info.get(AUDIO_COLUMN_COMPOSER)
                if info.get(AUDIO_COLUMN_MCDI):
                    self.mcdi = str(info.get(AUDIO_COLUMN_MCDI))
                if info.get(AUDIO_COLUMN_DATE):
                    self.publish_date = str(info.get(AUDIO_COLUMN_DATE))
                self.set_track(info.get(AUDIO_COLUMN_TRACK))

            self.duration = get_audio_duration(file)

    def set_track(self, value):
        try:
            self.track_int = int(value)
        except:
            try:
                num = ''.join([x for x in value if x.isdigit()])
                if num != '':
                    self.track_int = int(num)
            except:
                pass
            pass
        finally:
            self.track_str = value

    def __str__(self):
        return str(self.__class__.__name__) + ": " + str(self.__dict__)

    def create(self):
        db.session.add(self)

    def delete(self):
        db.session.delete(self)

    def update(self):
        db.session.commit()

    def create_with_commit(self):
        self.create()
        self.update()

    def delete_with_commit(self):
        self.delete()
        self.update()

    def as_dict(self):
        return {
            "id": self.id,
            "labels": self.labels,
            "track_int": self.track_int,
            "file_path": self.file_path,
            "song": self.song,
            "album": self.album,
            "artist": self.artist,
            "album_artis": self.album_artis,
            "genre": self.genre,
            "composer": self.composer,
            "publish_date": self.publish_date,
            "duration": self.duration,
        }


def create_song(file):
    if file and isinstance(file, str):
        info = get_audio_tag(file)
        if any(info):
            default_track_int = 1
            try:
                default_track_int = int(info.get(AUDIO_COLUMN_TRACK))
            except:
                try:
                    num = ''.join([x for x in info.get(AUDIO_COLUMN_TRACK) if x.isdigit()])
                    if num != '':
                        default_track_int = int(num)
                except:
                    pass

            return Song(
                id=None,
                labels=None,
                file_path=file,
                track_str=info.get(AUDIO_COLUMN_TRACK),
                track_int=default_track_int,
                song=info.get(AUDIO_COLUMN_SONG),
                album=info.get(AUDIO_COLUMN_ALBUM),
                artist=info.get(AUDIO_COLUMN_ARTIST),
                album_artis=info.get(AUDIO_COLUMN_ALBUM_ARTIST),
                genre=info.get(AUDIO_COLUMN_GENRE),
                wm_id=str(info.get(AUDIO_COLUMN_WM_ID)),
                composer=info.get(AUDIO_COLUMN_COMPOSER),
                mcdi=str(info.get(AUDIO_COLUMN_MCDI)),
                publish_date=str(info.get(AUDIO_COLUMN_DATE)),
                duration=get_audio_duration(file),
                file_update_date=parse(time.ctime(os.path.getmtime(file))),
                update_date=None
            )

    return None
