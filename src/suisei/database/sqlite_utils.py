import sqlite3

from suisei.common.log_utils import get_logger

_logger = get_logger(__name__)

create_audio_table_sql = """
    CREATE TABLE IF NOT EXISTS audio (
        `id` integer PRIMARY KEY AUTOINCREMENT,
        `labels` text,
        `track_str` text,
        `track_int` integer,
        `file_path` text,
        `song` text,
        `album` text,
        `artist` text,
        `album_artis` text,
        `genre` text,
        `wm_id` text,
        `composer` text,
        `mcdi` text,
        `date` text,
        `file_update_date` timestamp,
        `duration` integer 
    )
"""

create_user_table_sql = """
    CREATE TABLE IF NOT EXISTS user (
        `id` integer PRIMARY KEY AUTOINCREMENT,
        `name` text,
        `total_labels` text
    )
    
"""


def init_database(local_db_path):
    con = sqlite3.connect(local_db_path)
    con.set_trace_callback(print)
    try:
        create_database(con)
    except Exception as ex:
        _logger.warn(f'Db {local_db_path} error:', ex)

    finally:
        con.close()


def create_database(con):
    cur = con.cursor()

    cur.execute(create_audio_table_sql)
    cur.execute(create_user_table_sql)

    con.commit()


def paginate(con, query, execute_fn):
    cur = con.cursor()

    offset = 0
    limit = 1000

    try:
        while True:
            records = cur.execute(query, (offset, limit)).fetchall()

            if records and len(records) > 0:
                execute_fn(con, records)
                offset += limit
            else:
                break

    except Exception as ex:
        _logger.error(f'Run paginate sql {query} error : ', ex)
        raise ex
