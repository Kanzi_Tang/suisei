server_host = '0.0.0.0'
server_port = '5000'

server_template = 'anemachi/templates'
server_static = 'anemachi/static'

default_song_file_path = 'D:/OneDrive/Music/'

local_database_path = 'sqlite:///resource/suisei.db'

cpu_pool_num = 2