replace_prefix = '%{'
replace_suffix = '}'
COMMA = ','


def replace(text, parameter):
    new_text = str(text)
    for key, value in parameter.items():
        new_text = new_text.replace(replace_prefix + key + replace_suffix, value)

    return new_text


def list_join_to_string(symbol, lists):
    if lists and len(lists) > 0:
        return symbol.join(lists)
    else:
        return lists


def split_to_list(symbol, input_str):
    return input_str.split(symbol)
