import math
import os

import pandas

from suisei.common.log_utils import get_logger

_logger = get_logger(__name__)


def get_csv_file(file_path):
    if file_path.endswith(".csv") and os.path.isfile(file_path):
        df = pandas.read_csv(file_path, index_col=False)
        if not df.empty:
            return df
    else:
        _logger.warn(f'Not find csv file : {file_path}')

    return None


def save_to_csv(file_path, data_frame):
    data_frame.to_csv(file_path, encoding='utf-8', index=False)
    _logger.info(f'Save csv file done : {file_path}')


def generate_data_frame(columns, audio_list):
    return pandas.DataFrame(columns=columns, data=audio_list)


def get_column_value(data, column):
    value = data[column]

    if isinstance(value, float):
        if math.isnan(value):
            return None

    return value
