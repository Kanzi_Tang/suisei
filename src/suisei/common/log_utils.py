import logging
import sys

LOGGING_FORMAT = '[%(asctime)s][%(levelname)-8s][%(name)s:%(lineno)d]: %(message)s'

logging.basicConfig(stream=sys.stdout, format=LOGGING_FORMAT, level=logging.DEBUG)


def get_logger(name):
    if hasattr(name, '__module__') and hasattr(name, '__name__'):
        logger = logging.getLogger(name.__module__ + '.' + name.__name__)
    else:
        logger = logging.getLogger(str(name))

    return logger
