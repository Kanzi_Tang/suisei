from collections import Sequence

from pandas import DataFrame

from suisei.audio.audio_utils import AUDIO_COLUMN_LABELS
from suisei.common.log_utils import get_logger

_logger = get_logger(__name__)

PROPERTY_LABELS = 'labels'


def add_labels(labels, new_labels):
    if not labels:
        labels = set()

    if isinstance(new_labels, Sequence) and not isinstance(new_labels, str):
        for item in new_labels:
            labels.add(item)
    else:
        if isinstance(new_labels, set):
            labels.update(new_labels)
        else:
            labels.add(new_labels)

    return labels


def delete_labels(labels, new_labels):
    if labels:
        if isinstance(new_labels, Sequence) and not isinstance(new_labels, str):
            for item in new_labels:
                labels.discard(item)
        else:
            if isinstance(new_labels, set):
                labels = labels - new_labels
            else:
                labels.discard(new_labels)

        if len(labels) == 0:
            return None

    return labels


def data_frame_to_label_set(data_frame):
    try:
        temp = data_frame[AUDIO_COLUMN_LABELS]
        if temp and isinstance(temp, str):
            return eval(temp)
    except Exception as ex:
        _logger.warn(f'Set labels error : ', ex)
        return None


def get_all_labels(inputs):
    if inputs:
        if isinstance(inputs, list):
            return get_all_labels_by_audios(inputs)

        if isinstance(inputs, DataFrame):
            return get_all_labels_by_data_frame(inputs)

    _logger.warn(f'Unknown inputs type: {type(inputs)}')
    return None


def get_all_labels_by_data_frame(data_frame):
    try:
        column_labels = data_frame[AUDIO_COLUMN_LABELS].tolist()
        labels_list = [eval(labels) for labels in column_labels if labels and isinstance(labels, str)]

        all_labels = set()
        for labels in labels_list:
            all_labels.update(labels)

        return list(all_labels)
    except Exception as ex:
        _logger.warn(f'Data frame get all labels error : ', ex)
        return None


def get_all_labels_by_audios(audios):
    try:
        labels_list = [audio.labels for audio in audios if audio.labels]

        all_labels = set()
        for labels in labels_list:
            all_labels.update(labels)

        return list(all_labels)
    except Exception as ex:
        _logger.warn(f'Audios get all labels error : ', ex)
        return None


def update_labels(labels, ad_labels, de_labels):
    if len(ad_labels) > 0:
        labels = add_labels(labels, ad_labels)
        _logger.info(f"Add labels : {ad_labels}")

    if len(de_labels) > 0:
        labels = delete_labels(labels, de_labels)
        _logger.info(f"Delete labels : {de_labels}")

    return labels
