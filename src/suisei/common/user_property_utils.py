from suisei.common.json_utils import load_json
from suisei.common.json_utils import save_to_json
from suisei.common.label_untils import PROPERTY_LABELS


def save_total_labels_as_json(filename, labels):
    json = {
        PROPERTY_LABELS: labels
    }
    save_to_json(filename, json)


def get_total_labels_from_property(filename):
    user_property = load_json(filename)
    if user_property:
        record_total_labels = user_property.get(PROPERTY_LABELS)
        if any(record_total_labels):
            return record_total_labels

    return None
