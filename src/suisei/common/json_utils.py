import json
import os

from suisei.common.log_utils import get_logger

_logger = get_logger(__name__)


def save_to_json(file_name, data):
    with open(file_name, 'w') as file:
        json.dump(data, file)
    _logger.info(f'Save json file done: {file_name}')


def load_json(file_name):
    if os.path.isfile(file_name):
        with open(file_name, 'r') as obj:
            data = json.load(obj)
        _logger.info(f'Load json file done: {file_name}')
        return data
    else:
        _logger.warn(f'Json file not fine: {file_name}')
        return None
