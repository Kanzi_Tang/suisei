from enum import Enum


class AudioType(Enum):
    MP3 = '.mp3'
    AAC = '.aac'
    FLAC = '.flac'
    ALAC = '.alac'
    OGG = '.ogg'
    OPUS = '.opus'
    WAV = '.wav'
