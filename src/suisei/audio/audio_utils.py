import base64
import os
import uuid

import mutagen
from mutagen.flac import FLAC
from mutagen.flac import Picture
from mutagen.mp3 import MP3
from mutagen.oggvorbis import OggVorbis
from pandas import DataFrame

import librosa

from suisei.audio.audio_type import AudioType
from suisei.common.log_utils import get_logger

_logger = get_logger(__name__)

TAG_FLAC_SONG = 'title'
TAG_FLAC_ALBUM = 'album'
TAG_FLAC_ARTIST = 'artist'
TAG_FLAC_ALBUM_ARTIST = 'albumartist'
TAG_FLAC_GENRE = 'genre'
TAG_FLAC_WMID = 'wm/wmcollectionid'
TAG_FLAC_COMPOSER = 'composer'
TAG_FLAC_MCDI = 'wm/mcdi'
TAG_FLAC_TRACK = 'tracknumber'
TAG_FLAC_DATE = 'date'
TAG_FLAC_IMAGE = 'image'

# ID3V2.4
TAG_MP3_SONG = 'TIT2'
TAG_MP3_ALBUM = 'TALB'
TAG_MP3_ARTIST = 'TPE1'
TAG_MP3_ALBUM_ARTIST = 'TPE2'
TAG_MP3_GENRE = 'TCON'
TAG_MP3_WMID = 'PRIV:WM/WMCollectionID'
TAG_MP3_COMPOSER = 'TCOM'
TAG_MP3_MCDI = 'MCDI'
TAG_MP3_TRACK = 'TRCK'
TAG_MP3_DATE = 'TDRC'
TAG_MP3_IMAGE = 'APIC'

TAG_OGG_SONG = 'title'
TAG_OGG_ALBUM = 'Album'
TAG_OGG_ARTIST = 'ARTIST'
TAG_OGG_ALBUM_ARTIST = 'ALBUMARTIST'
TAG_OGG_GENRE = 'Genre'
TAG_OGG_WMID = 'WM/WMCOLLECTIONID'
TAG_OGG_COMPOSER = 'COMPOSER'
TAG_OGG_MCDI = 'WM/MCDI'
TAG_OGG_TRACK = 'TRACKNUMBER'
TAG_OGG_DATE = 'DATE'
TAG_OGG_IMAGE = 'METADATA_BLOCK_PICTURE'

# System column
AUDIO_COLUMN_ID = 'id'
AUDIO_COLUMN_PATH = 'path'
AUDIO_COLUMN_SONG = 'song'
AUDIO_COLUMN_ALBUM = 'album'
AUDIO_COLUMN_ARTIST = 'artist'
AUDIO_COLUMN_ALBUM_ARTIST = 'album_artist'
AUDIO_COLUMN_GENRE = 'genre'
AUDIO_COLUMN_WM_ID = 'wm_id'
AUDIO_COLUMN_COMPOSER = 'composer'
AUDIO_COLUMN_MCDI = 'mcdi'
AUDIO_COLUMN_DATE = 'date'
AUDIO_COLUMN_LABELS = 'labels'
AUDIO_COLUMN_TRACK = 'track'

AUDIO_COLUMN_IMAGE = 'image'

INFO_COLUMN_MAPS = {
    AUDIO_COLUMN_SONG: [TAG_FLAC_SONG, TAG_MP3_SONG, TAG_OGG_SONG],
    AUDIO_COLUMN_ALBUM: [TAG_FLAC_ALBUM, TAG_MP3_ALBUM, TAG_OGG_ALBUM],
    AUDIO_COLUMN_ARTIST: [TAG_FLAC_ARTIST, TAG_MP3_ARTIST, TAG_OGG_ARTIST],
    AUDIO_COLUMN_ALBUM_ARTIST: [TAG_FLAC_ALBUM_ARTIST, TAG_MP3_ALBUM_ARTIST, TAG_OGG_ALBUM_ARTIST],
    AUDIO_COLUMN_GENRE: [TAG_FLAC_GENRE, TAG_MP3_GENRE, TAG_OGG_GENRE],
    AUDIO_COLUMN_WM_ID: [TAG_FLAC_WMID, TAG_MP3_WMID, TAG_OGG_WMID],
    AUDIO_COLUMN_COMPOSER: [TAG_FLAC_COMPOSER, TAG_MP3_COMPOSER, TAG_OGG_COMPOSER],
    AUDIO_COLUMN_MCDI: [TAG_FLAC_MCDI, TAG_MP3_MCDI, TAG_OGG_MCDI],
    AUDIO_COLUMN_DATE: [TAG_FLAC_DATE, TAG_MP3_DATE, TAG_OGG_DATE],
    AUDIO_COLUMN_TRACK: [TAG_FLAC_TRACK, TAG_MP3_TRACK, TAG_OGG_TRACK],
    AUDIO_COLUMN_IMAGE: [TAG_FLAC_IMAGE, TAG_MP3_IMAGE, TAG_OGG_IMAGE]
}

AUDIO_COLUMNS = [
    AUDIO_COLUMN_ID,
    AUDIO_COLUMN_PATH,
    AUDIO_COLUMN_SONG,
    AUDIO_COLUMN_ALBUM,
    AUDIO_COLUMN_ARTIST,
    AUDIO_COLUMN_ALBUM_ARTIST,
    AUDIO_COLUMN_GENRE,
    AUDIO_COLUMN_WM_ID,
    AUDIO_COLUMN_COMPOSER,
    AUDIO_COLUMN_MCDI,
    AUDIO_COLUMN_DATE,
    AUDIO_COLUMN_LABELS,
    AUDIO_COLUMN_TRACK
]


def get_audio_files(path='/'):
    files = [os.path.join(r, file) for r, d, f in os.walk(path) for file in f]
    types = [str(audio.value) for audio in AudioType]

    def check_audio_file(file):
        root, extension = os.path.splitext(file)
        return extension in types

    return list(filter(check_audio_file, files))


def is_audio_file(file):
    types = [str(audio.value) for audio in AudioType]
    root, extension = os.path.splitext(file)
    return extension in types


def get_audio_tag(file_path):
    tags = mutagen.File(file_path)
    if isinstance(tags, FLAC):
        return parse_flac_tag(tags)
    elif isinstance(tags, OggVorbis):
        return parse_ogg_tag(tags)
    else:
        return parse_mp3_tag(tags)


def parse_flac_tag(tags):
    info = {}
    for key, value in tags.items():
        try:
            column = convert_audio_column_name(key)
            if column:
                info[column] = value[0]

        except Exception as ex:
            _logger.error(f'{tags.pprint()} get flac tag error : ', ex)
            info[key] = None
    return info


def parse_ogg_tag(tags):
    info = {}
    for key, value in tags.items():
        try:
            column = convert_audio_column_name(key)
            if column:
                if column is AUDIO_COLUMN_WM_ID:
                    info[column] = ''.join(filter(str.isalnum, value[0]))
                else:
                    info[column] = value[0]

        except Exception as ex:
            _logger.error(f'{tags.pprint()} get flac tag error : ', ex)
            info[key] = None
    return info


def parse_mp3_tag(tags):
    info = {}
    for key, value in tags.items():
        try:
            column = convert_audio_column_name(key)
            if column:
                if column is AUDIO_COLUMN_IMAGE:
                    continue

                if column in [AUDIO_COLUMN_MCDI, AUDIO_COLUMN_WM_ID]:
                    info[column] = value.data
                elif AUDIO_COLUMN_WM_ID in column:
                    info[column] = value.data
                else:
                    info[column] = value.text[0]

        except Exception as ex:
            _logger.error(f'{tags.pprint()} get mp3 tag error : ', ex)
            info[key] = None
    return info


def convert_audio_column_name(tag_key):
    for key, value in INFO_COLUMN_MAPS.items():
        if tag_key in value:
            return key

    if TAG_MP3_WMID in tag_key:
        return AUDIO_COLUMN_WM_ID
    return None


def merge_audios_with_data(audios, data_frame):
    for audio in audios:
        merge_audio_with_data_frame(audio, data_frame)
    return audios


def merge_audio_with_data_frame(audio, data_frame):
    if isinstance(data_frame, DataFrame):
        index_list = data_frame.index[data_frame[AUDIO_COLUMN_PATH] == audio.path].tolist()
        for index in index_list:
            audio_data = data_frame.loc[index]
            audio.id = audio_data[AUDIO_COLUMN_ID]
            audio.set_labels(audio_data)

    if not audio.id:
        audio.id = str(uuid.uuid4())
    return audio


def get_image_by_file(file):
    try:
        tags = mutagen.File(file)
        if isinstance(tags, FLAC):
            return {AUDIO_COLUMN_IMAGE: tags.pictures[0].data}
        elif isinstance(tags, OggVorbis):
            image = tags.tags.get(TAG_OGG_IMAGE)[0]
            if any(image):
                decode = base64.b64decode(image)
                return {AUDIO_COLUMN_IMAGE: Picture(decode).data}
            else:
                return None
        else:
            cover = tags.tags.get(TAG_MP3_IMAGE)
            if cover:
                return {AUDIO_COLUMN_IMAGE: cover.data}
            else:
                return {AUDIO_COLUMN_IMAGE: tags.tags.getall(TAG_MP3_IMAGE)[0].data}
    except Exception as ex:
        _logger.error(f'Get image error : ', ex)
        return None


def group_by_albums(audios):
    albums = {}
    for audio in audios:
        album_name = audio.album
        if albums.get(album_name):
            song_list = albums.get(album_name)
            song_list.append(audio)
        else:
            albums[album_name] = [audio]

    sort_track_by_album(albums)
    return albums


def sort_track_by_album(albums):
    def get_track(s):
        if s.track_str:
            num = ''.join([x for x in s.track_str if x.isdigit()])
            if num != '':
                return int(num)

        return "9999999999999"

    for key, song_list in albums.items():
        albums[key] = sorted(song_list, key=get_track)


def filter_by_labels(audio_list, white_labels, black_labels):
    def match_white_labels(audio):
        if not audio.labels or len(audio.labels) == 0:
            return True

        for label in white_labels:
            if label in audio.labels:
                return True

        return False

    def match_black_labels(audio):
        for label in black_labels:
            if label in audio.labels:
                return False
        return True

    if white_labels and len(white_labels) > 0:
        audio_list = list(filter(match_white_labels, audio_list))
    if black_labels and len(black_labels) > 0:
        audio_list = list(filter(match_black_labels, audio_list))

    return audio_list


def get_audio_duration(file_path):
    try:
        audio_file = librosa.get_duration(filename=file_path)
        if audio_file:
            return int(audio_file)
        else:
            return 0
    except Exception as ex:
        audio_info = MP3(file_path).info
        return int(audio_info.length)
